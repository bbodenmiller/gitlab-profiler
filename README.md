## Introduction

The goal of gitlab-profiler is to help engineers understand why specific
URLs may be slow and provide hard data that can help reduce load times.

For GitLab.com, you can find the latest results here:

http://redash.gitlab.com/dashboard/gitlab-profiler-statistics

## What It Does

gitlab-profiler currently exposes a multi-URL profiler `gitlab_profiler.rb`,
which can be configured to monitor multiple URLs and store the data. The
multi-URL profiler does the following:

1. Profiles a set URLs directly on a GitLab installation under ruby-prof using
   [`bin/profile-url`](https://docs.gitlab.com/ee/development/profiling.html#profiling-a-url)
   in the GitLab installation.
2. Generates the ruby-prof call stack data into a HTML file.
3. Log the SQL timing data into a separate file.
4. Upload the HTML and timing data into S3.
5. Log the results into a PostgreSQL database.

## How This Has Helped

A wise programmer who implemented the [Fastest Fourier Transfer in the West]
(https://en.wikipedia.org/wiki/FFTW) once told me, "Always look at the compiler
output." In the same way one might examine the assembly code generated from a C++
compiler, one should also look at all the SQL queries that Rails generates.

By looking at the SQL dumps we can find surprising queries that are
not properly cached:

https://gitlab.com/gitlab-org/gitlab-ee/issues/1644

In addition, [by sorting the queries by load
times](https://gitlab.com/gitlab-org/gitlab-ce/snippets/1548079), we can
clearly identify queries that [could benefit from missing
indicies](https://gitlab.com/gitlab-org/gitlab-ce/issues/27676).

By looking at the ruby-prof output, one can better understand what specifically
needs to be optimized to reduce load times.

## How to Contribute

If you have more URLs that you want to profile for GitLab.com, submit a merge request to
[the sample configuration](https://gitlab.com/gitlab-com/gitlab-profiler/blob/master/config.yml.sample)
file.

## Requirements

NOTE: you must have access to a production GitLab machine to run this
profiler.  While not everyone at GitLab may have access to do this, the
results of the profiler we run at GitLab should be accessible to all.

Simple URL Profiler:
1. `sudo` access on the machine
2. GitLab installed on the machine via omnibus

Multi-URL Profiler:
1. awscli configured properly (e.g. `apt-get install awscli`)
2. `sudo` access on the machine
3. GitLab installed on the machine via omnibus
4. Gems in `Gemfile.lock` (you may need to install ruby-dev and libpq-dev)

## Multi-URL Profiler Configuration

To install the Multi-URL Profiler on Ubuntu:

```
apt-get install ruby-dev libpq-dev ruby ruby-bundler awscli
bundle install --path .bundle
aws configure # Enter in your AWS credentials
cp config.yml.sample config.yml
```

Edit `config.yml` with the proper data. Once this is configured, you can run:

```
bundle exec ruby gitlab_profiler.rb config.yml
```

### Config Format

A sample `config.yml.example` contains a example configuration file.
Ths file has four different sections:

### Storage Config: `storage`

This contains the information necessary to store the profile data and make
it easy for users to access that information.

### Database Config: `database`

This contains the configuration to store the results of the profile data.

### GitLab Authentication Config: `auth`

This contains the API private tokens needed to access GitLab with different
levels of access. For example, you may want to profile a URL as a Master (as
defined by GitLab permissions).

### Profiler Config: `profiler`

This contains the URLs that should be profiled and what level of
authentication should be used for each URL. By default, all URLs are queried as
an anonymous user.

## Syntax

To run the multi-URL profiler:

```sh
bundle exec ruby gitlab_profiler config.yml
```

## Troubleshooting

### Permissions

The scripts included here need to be run in a directory where the user `git`
can read them (e.g. `/tmp`).
